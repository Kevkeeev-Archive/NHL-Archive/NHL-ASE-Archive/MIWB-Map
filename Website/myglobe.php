<?php
include_once('config.php');
if(!isset($_SESSION)){ session_start(); }
if(!isset($_SESSION['email'])){ header('Location: ' . $homepage); }
if(!isset($_GET['user']) || $_GET['user'] == ""){ $_GET['user'] = $_SESSION['email']; }

$con = connect();
$result = mysqli_query($con, "SELECT Naam FROM Student WHERE Studentnummer = '". mysqli_real_escape_string($con, $_GET['user']) ."' OR Emailaddress = '". $_SESSION['email'] ."' ");
$username = mysqli_fetch_row($result)[0];

$stages = mysqli_query($con, 
	"SELECT Stage.FromDate, Stage.ToDate, Schip.IMO, Schip.MMSI, Schip.Naam FROM Stage, Schip ".
	"WHERE Stage.MMSI = Schip.MMSI AND Stage.StudentEmailaddress = '". $_SESSION['email'] ."' ");

$logs = mysqli_query($con,
	"SELECT p.DateTime, p.Message, s.Naam From PaspoortEntries as p, Schip as s ".
	"WHERE p.MMSI = s.MMSI AND p.StudentEmailaddress = '". $_SESSION['email'] ."' ");

mysqli_close($con);
?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo $sitename . ' - ' . ($_SESSION['email'] == $_GET['user'] ? 'My' : $username . "'s"); ?>  Globe</title>
</head>

<body>

<center><h1><?php echo $username; ?>'s Globe!</h1></center>

< List of stages / >
<?php
	while ($row = mysqli_fetch_assoc($stages)) {
		printf (
			"<p>Stage on '%s' (IMO: %s, MMSI: %s) from %s to %s.</p>", 
			$row["Naam"], $row["IMO"], $row["MMSI"], 
			date_format(date_create_from_format("d-m-Y", $row["FromDate"]), "D j M Y"), 
			date_format(date_create_from_format("d-m-Y", $row["ToDate"]), "D j M Y") 
		);
    }

?>

<form action="newstage.php">
<input type="submit" value="New Stage!" />
</form>

<br><br>

< List of Paspoort entries / >
<?php
	while ($row = mysqli_fetch_assoc($logs)) {
		printf (
			"<p>Log from %s on the %s: %s</p>", 
			date_format(date_create_from_format("d-m-Y", $row["DateTime"]), "D j M Y"), 
			$row["Naam"],
			mb_substr($row["Message"], 0, 20) //only 20 chars
		);
    }
?>
<form action="newmessage.php">
<input type="submit" value="New Log! <STUK>" />
</form>


</body>
</html>
