CREATE TABLE MIWB.Gebruiker (
    Emailaddress varchar(255) NOT NULL,
	Password varchar(255) NOT NULL,
    UNIQUE (Emailaddress),
	PRIMARY KEY (Emailaddress)
);

CREATE TABLE MIWB.Student (
	Emailaddress varchar(255) NOT NULL,
	Naam varchar(255) NOT NULL,
	Studentnummer int,
	Foto varchar(255),
	UNIQUE (Emailaddress),
	PRIMARY KEY (Emailaddress)
);

CREATE TABLE MIWB.Schip (
	IMO varchar(255) NOT NULL,
	MMSI varchar(255) NOT NULL,
	Naam varchar(255) NOT NULL,
	Longitude varchar(255) NOT NULL,
	Latitude varchar(255) NOT NULL,
	Foto varchar(255),
	UNIQUE (MMSI),
	PRIMARY KEY (MMSI)
);

CREATE TABLE MIWB.Positie (
	MMSI varchar(255) NOT NULL,
	DateTime varchar(255) NOT NULL,
	Longitude varchar(255) NOT NULL,
	Latitude varchar(255) NOT NULL,
	CONSTRAINT UC_Pos UNIQUE (MMSI,DateTime)
);

CREATE TABLE MIWB.Stage (
	StudentEmailaddress varchar(255) NOT NULL,
	MMSI varchar(255) NOT NULL,
	fromDate varchar(255) NOT NULL,
	toDate varchar(255) NOT NULL
);

CREATE TABLE MIWB.PaspoortEntries (
	StudentEmailaddress varchar(255) NOT NULL,
	MMSI varchar(255) NOT NULL,
	DateTime varchar(255) NOT NULL,
	Longitude varchar(255) NOT NULL,
	Latitude varchar(255) NOT NULL,
	Message varchar(255) NOT NULL,
	CONSTRAINT UC_PaspoortEntry UNIQUE (StudentEmailaddress, MMSI, DateTime)
);