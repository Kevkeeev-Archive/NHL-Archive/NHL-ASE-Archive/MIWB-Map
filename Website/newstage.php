<?php
include_once('config.php');
if(!isset($_SESSION)){ session_start(); }
if(!isset($_SESSION['email'])){ header('Location: ' . $homepage); }
if(!isset($_GET['user']) || $_GET['user'] == ""){ $_GET['user'] = $_SESSION['email']; }

if( isset($_POST['mode']) && $_POST['mode'] == "addStage" ) {

	$con = connect();

	mysqli_query($con, "INSERT INTO Schip (IMO, MMSI) VALUES ( '". mysqli_real_escape_string($con, $_POST['IMO']) ."', '". mysqli_real_escape_string($con, $_POST['MMSI']) ."')"); 
	
	$resultShip = mysqli_query($con, "SELECT MMSI FROM Schip WHERE IMO = '". mysqli_real_escape_string($con, $_POST['IMO']) ."' OR MMSI = '".  mysqli_real_escape_string($con, $_POST['MMSI']) ."' ");
	$shipMMSI = mysqli_fetch_row($resultShip)[0];
	
	$result = mysqli_query($con, "INSERT INTO Stage (StudentEmailaddress, MMSI, fromDate, toDate) VALUES ('". $_SESSION['email'] ."', ". $shipMMSI .", '". mysqli_real_escape_string($con, $_POST['fromDate']) ."', '". mysqli_real_escape_string($con, $_POST['toDate']) ."') ");

	mysqli_close($con);
	header('Location: myglobe.php');
}
	
?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo $sitename; ?> - New Stage</title>
</head>

<body>
<p>Hello <?php echo $_SESSION['email']; ?>, please enter the details of a new stage.</p>

<form action="" method="post">
<input type="hidden" name="mode" value="addStage" />
<p>From: <input type="date" name="fromDate" value="<?php echo date('d-m-Y'); ?>" /></p>
<p>To:   <input type="date" name="toDate"   value="<?php echo date('d-m-Y'); ?>" /></p>
<p>IMO:  <input type="text" name="IMO" /> or MMSI: <input type="text" name="MMSI" /></p>
<!-- ship foto -->
<input type="submit" value="Send" />
</form>




</body>
</html>
