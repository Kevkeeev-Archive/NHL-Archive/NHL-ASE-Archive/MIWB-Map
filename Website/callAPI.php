<?php
include_once('config.php');

// https://stackoverflow.com/a/9802854
function callAPI($method, $url, $data = false) {
    $curl = curl_init();

    switch ($method) {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    //curl_setopt($curl, CURLOPT_USERPWD, "username:password");

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}

//Unused, Positions now goes by MMSI
function getSchipIDByMMSI($con, $mmsi){
	$res = mysqli_query($con, "SELECT ID FROM Schip WHERE MMSI = '". $mmsi ."' ");
	return mysqli_fetch_assoc($res)['ID'];
}

$url = $url . "&format=json";

$json = callAPI("GET", $url, false);
//echo 'Raw (prettified) result from API Call: <br>' . str_replace('\n', '<br>', json_encode($json, JSON_PRETTY_PRINT)) . '<br>';

$result = json_decode($json, true);
//echo 'Decoded result:<br>'; var_dump($result);

$pushToDB = true;
if($pushToDB) { //false if testing, true if connected to actual AIS database
	$con = connect();

	$size = count($result);
	for($i=0; $i<$size; $i++){
		//process each row of the result
		
		//add the position to the Position table for the route
		mysqli_query($con, 
			"INSERT INTO Positie (MMSI, DateTime, Longitude, Latitude) ".
			"VALUES ( '". $result[$i]["AIS"]["MMSI"] ."', '". $result[$i]["AIS"]["TIMESTAMP"] ."', '". 
			     $result[$i]["AIS"]["LONGITUDE"] ."', '". $result[$i]["AIS"]["LATITUDE"] ."' )"
		);
		
		// insert the new ship's info, or if it exists, update it.
		mysqli_query($con,
			"INSERT INTO Schip (IMO, MMSI, Naam, Latitude, Longitude, Foto) ".
			"VALUES ( '". $result[$i]["AIS"]["IMO"] ."', '". $result[$i]["AIS"]["MMSI"] ."', '".
				$result[$i]["AIS"]["NAME"] ."', '". $result[$i]["AIS"]["LATITUDE"] ."', '". 
				$result[$i]["AIS"]["LONGITUDE"] ."', 'boot.jpg' )" .
			
			"ON DUPLICATE KEY UPDATE ".
			     	"IMO = '". $result[$i]["AIS"]["IMO"] ."', Naam = '". $result[$i]["AIS"]["NAME"] 
			     	."', Longitude = '". $result[$i]["AIS"]["LONGITUDE"] ."', Latitude = '". 
			     	$result[$i]["AIS"]["LATITUDE"] ."', Foto = 'boot.jpg' "
		);
		
	}
	echo 'inserted '.$i.' results in DB.'; 
	mysqli_close($con);
}

?>
