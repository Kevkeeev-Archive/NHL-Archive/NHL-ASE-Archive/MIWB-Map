<?php
include_once('config.php');
if(!isset($_SESSION)){ session_start(); }

if(isset($_POST['mode']) && $_POST['mode'] != "" ) {
	echo '<script>console.log("Mode set.")</script>';

	$con = connect();
	
	if( $_POST['mode'] == "login"){
		//Do login
		
		if(
			isset($_POST['email']) && $_POST['email'] != "" &&
			isset($_POST['password']) && $_POST['password'] != ""
		) {
			$return = mysqli_query($con, "SELECT Emailaddress, Password FROM Gebruiker WHERE Emailaddress = '". mysqli_real_escape_string($con, $_POST['email'])."' ");
			
			if(mysqli_num_rows($return) > 0) {
				$tmp = mysqli_fetch_row($return);

				if(password_verify(mysqli_real_escape_string($con, $_POST['password']), $tmp[1]) ) {
					$_SESSION['email'] = $tmp[0];
					header('Location: myglobe.php');
				} else { //password doesnt match
				
				}
			} else {//no user found with emailaddress

			}
		} else { //empty field(s)

		}
		
		
	} else if ( $_POST['mode'] == "register") {
		//Do register
		
		if(
			isset($_POST['email']) && $_POST['email'] != "" &&
			isset($_POST['password1']) && $_POST['password1'] != "" &&
			isset($_POST['password2']) && $_POST['password2'] != ""
		) {
			$return = mysqli_query($con, "SELECT Emailaddress FROM Gebruiker WHERE Emailaddress = '". mysqli_real_escape_string($con, $_POST['email']) ."'");
			
			if($_POST['password1'] == $_POST['password2'] && (! $return || mysqli_num_rows($return) <= 0) ) {
				$return = mysqli_query($con, "INSERT INTO Gebruiker (Emailaddress, Password) VALUES ('". mysqli_real_escape_string($con, $_POST['email']) ."', '". password_hash(mysqli_real_escape_string($con, $_POST['password1']), PASSWORD_DEFAULT) ."')");
								
				$_SESSION['email'] = mysqli_real_escape_string($con, $_POST['email']);
				header('Location: myglobe.php');
			} else {
				//not equal passwords, or already used emailaddress
			}
		} else {
			//empty field(s)
		}
	}
	
	unset($_POST['mode']);
	mysqli_close(connect());
} else {
	echo '<script>console.log("Mode not set.")</script>';
	echo 'Mode not set';
}


?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo $sitename; ?> - Login</title>
</head>

<body>

<!-- login -->
<form action = "" method="post">
<input type="hidden" name="mode" value="login" />
<p>@: <input type="email" name="email" /></p>
<p>*: <input type="password" name="password" /></p>
<p><input type="submit" value="Log in!"></p>

</form>

<hr>

<!-- register -->
<form action = "" method="post">
<input type="hidden" name="mode" value="register" />
<p>@: <input type="email" name="email" /></p>
<p>*: <input type="password" name="password1" /></p>
<p>*: <input type="password" name="password2" /></p>
<p><input type="submit" value="Register!"></p>

</form>


</body>
</html>
