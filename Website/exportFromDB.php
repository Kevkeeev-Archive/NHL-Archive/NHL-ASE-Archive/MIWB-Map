<?php
include_once('config.php');

if(! isset($_GET['mode']) || $_GET['mode'] == "") {
	echo 'ERROR: ?mode not set!';
} else if( $_GET['mode'] == "positions" || $_GET['mode'] == "ships") { 
	$con = connect();

	if($_GET['mode'] == "positions") {
		$return = mysqli_query($con, 
			"SELECT MMSI, Latitude, Longitude FROM Positie ORDER BY MMSI ASC, DateTime ASC"
		);
	} else if( $_GET['mode'] == "ships" ) {
		$return = mysqli_query($con,
			"SELECT * FROM Schip ORDER BY MMSI ASC"
		);
	}
	
	while ($row = mysqli_fetch_assoc($return)) {
		if($_GET['mode'] == "positions"){
			echo $row['MMSI'] . "|" . $row['Latitude'] . "|" . $row['Longitude'] . ',';
		} else if ($_GET['mode'] == "ships" ) {
			echo $row['MMSI'] . "|" . $row['IMO'] . "|" . $row['Naam'] . "|" . $row['Latitude'] . "|" 
				. $row['Longitude'] . "|" . $row['Foto'] . ',';
		}
    }
	
	mysqli_close($con);
}

?>

