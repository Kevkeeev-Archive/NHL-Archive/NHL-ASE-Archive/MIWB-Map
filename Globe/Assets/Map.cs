﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

class Position {
	public string MMSI;
	public float lat;
	public float lon;

	public Position(string MMSI, float lat, float lon){
		this.MMSI = MMSI;
		this.lat = lat;
		this.lon = lon;
	}
}

class Ship {
	public string MMSI;
	public string IMO;
	public string Name;
	public float lat;
	public float lon;
	public string PhotoURL;

	public Ship(string MMSI, string IMO, string Name, float lat, float lon, string PhotoURL){
		this.MMSI = MMSI;
		this.IMO = IMO;
		this.Name = Name;
		this.lat = lat;
		this.lon = lon;
		this.PhotoURL = PhotoURL;
	}
}

public class Map : MonoBehaviour {

	[System.Serializable]
	public struct LatLon { //students
		public float lat;
		public float lon;
		public string name;
	};
	public bool generateDebugStudents = false;
	public bool showDebugStudents = false;
	public List<LatLon> students;
	public GameObject studentPrefab;

	public GameObject positionPrefab;
	public GameObject shipPrefab;
	public Transform earthTransform;
	public float earthRadius;

	/**
	 * Function that converts the given (lat,lon) position into an (x,y,z) coordinate.
	 * Uses altitude and radius of the Earth Sphere as additional parameters.
	 */
	Vector3 llarToWorld(float lat, float lon, float alt, float rad) {
		lat *= 0.0174533f;	// Convert to radians.
		lon *= 0.0174533f;
		
		float f = 0;
		float ls = Mathf.Atan((1.0f - f) * 2.0f * Mathf.Tan(lat));

		float x = rad * Mathf.Cos(ls) * Mathf.Cos(lon) + alt * Mathf.Cos(lat) * Mathf.Cos(lon);
		float y = rad * Mathf.Cos(ls) * Mathf.Sin(lon) + alt * Mathf.Cos(lat) * Mathf.Sin(lon);
		float z = rad * Mathf.Sin(ls) + alt * Mathf.Sin(lat);

		return new Vector3(x, z, y);
	}

	// Use this for initialization
	IEnumerator Start () {
		if(generateDebugStudents) {
			for(float a = -90; a < 90; a += 2.0f) {

				for(float b = -180; b < 180; b += 2.0f) {
					LatLon l = new LatLon();
					l.lat = a;
					l.lon = b;
					l.name = Guid.NewGuid().ToString();

					students.Add(l);
				}
			}
		}
		if(showDebugStudents){
			foreach(var s in students) {
				var v3 = llarToWorld(s.lat, s.lon, 0.0f, earthRadius);

				GameObject g = Object.Instantiate<GameObject>(studentPrefab);
				g.transform.position = v3;
				g.name = s.name;
				g.transform.parent = earthTransform;
			}
		}

		var url = "http://diamundo.nl/MIWB/exportFromDB.php";
		//This points to the script where Unity can read from the database.

		List<Position> knownPositions = new List<Position> ();
		{ //new scope to process local API
			WWW www = new WWW (url + "?mode=positions");
			yield return www;

			string[] positions = www.text.Substring(0, www.text.Length - 1).Split(','); // chop off the trailing comma
			//string[] positions = www.text.Split (','); 
			for (int i = 0; i < positions.Length-1; i++) {
				if (positions [i] == string.Empty) { continue; } //if a position is empty, skip it

				string[] tmp = positions[i].Split('|');
				knownPositions.Add(new Position( tmp[0], float.Parse( tmp[1]), float.Parse( tmp[2]) ) );
			}
		}
		foreach(Position p in knownPositions){
			var v3 = llarToWorld(p.lat, p.lon, 0.0f, earthRadius);

			GameObject g = Object.Instantiate<GameObject>(positionPrefab);
			g.transform.position = v3;
			g.transform.parent = earthTransform;
		}

		List<Ship> knownShips = new List<Ship> ();
		{ //new scope to process local API
			WWW www = new WWW(url + "?mode=ships");
			yield return www;

			string[] ships = www.text.Substring(0, www.text.Length - 1).Split(','); // chop off the trailing comma
			//string[] ships = www.text.Split(','); 
			for(int i = 0; i < ships.Length-1; i++){
				string[] tmp = ships[i].Split('|');

				knownShips.Add(new Ship( tmp[0], tmp[1], tmp[2], float.Parse(tmp[3]), float.Parse(tmp[4]), tmp[5] ));
			}
		}
		foreach(Ship b in knownShips) { //b van boot :) {
			var v3 = llarToWorld(b.lat, b.lon, 0.0f, earthRadius);

			GameObject g = Object.Instantiate<GameObject>(shipPrefab);
			g.transform.position = v3;
			g.transform.parent = earthTransform;
		}

		studentPrefab.SetActive(false);
		positionPrefab.SetActive(false);
		shipPrefab.SetActive(false);
		
	}
	

	// Update is called once per frame
	void Update () {
		//nothing is needed to be done here for now, everything is loaded in.
		//If in the future, we want to reload the positions from time to time, that could be done here.
	}


}
